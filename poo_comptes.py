"""
Exercices POO Comptes bancaires
"""

import os
import csv
from datetime import datetime
from model import CompteCourant, CompteEpargne


def banque(compte):
    """opération bancaire pour la CLI"""
    print(
        f"Solde du compte n°{compte.numero_compte} de {compte.nom_proprietaire} = {compte.afficher_solde()}€.")

    montant = int(input("Sélectionnez un montant:"))

    try:
        if montant < 0:
            compte.retrait(montant)
            print(f"Retrait de {abs(montant)}€ en cours")
        else:
            compte.versement(montant)
            print(f"Versement de {montant}€ en cours")

        if type(compte) == CompteEpargne:
            print(f"Application des intérêts à hauteur de {compte.pourcentage_interets * 100}%")
            compte.appliquer_interets()
        elif type(compte) == CompteCourant and compte.afficher_solde() < 0:
            print(f"Application des agios à hauteur de {compte.pourcentage_agios * 100}%")
            compte.appliquer_agios()

        print(
            f"Solde du compte n°{compte.numero_compte} de {compte.nom_proprietaire} = {compte.afficher_solde()}€.")
        log_infos(compte)

    except ValueError as e:
        print(e)


def log_infos(compte):
    dir_name = "extraits_comptes"
    if dir_name not in os.listdir():
        os.mkdir(dir_name)

    with open(f"{dir_name}/mouvements_compte_{compte.numero_compte}.csv", mode="a", newline="") as log_file:
        file_writer = csv.writer(log_file)
        file_writer.writerow([compte.numero_compte, compte.nom_proprietaire, compte.afficher_solde(),
                              datetime.now().strftime("%d-%m-%Y %H:%M:%S")])


def operation(compte, montant):
    """Opération bancaire pour l'interface graphique"""
    if montant < 0:
        compte.retrait(montant)
    else :
        compte.versement(montant)

    if type(compte) == CompteEpargne:
        compte.appliquer_interets()

    elif type(compte) == CompteCourant and compte.afficher_solde() < 0:
        compte.appliquer_agios()
    log_infos(compte)
