"""
module principal pour la version avec interface graphique
"""
import constants
from model import CompteCourant, CompteEpargne
from view import AppliBanque

mon_compte_courant = CompteCourant(numero_compte='12345', nom_proprietaire="DOUDOUX", solde=100,
                                   autorisation_decouvert=50,
                                   pourcentage_agios=constants.POURCENTAGE_AGIOS)
mon_compte_epargne = CompteEpargne(numero_compte='09876', nom_proprietaire="ABITBOL",
                                   solde=200, pourcentage_interets=constants.POURCENTAGE_INTERETS)

# Commenter/décommenter le compte à tester:

app = AppliBanque(mon_compte_courant)
# app = AppliBanque(mon_compte_epargne)

if __name__ == '__main__':
    app.mainloop()
