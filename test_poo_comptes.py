"""
tests de l'exercice
"""
import unittest
from unittest.mock import patch
from datetime import datetime
import constants
from model import CompteCourant, CompteEpargne
from poo_comptes import banque


class ComptesTestClass(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.mon_compte_courant = CompteCourant(numero_compte='12345', nom_proprietaire="DOUDOUX", solde=100,
                                                autorisation_decouvert=50,
                                                pourcentage_agios=constants.POURCENTAGE_AGIOS)
        self.mon_compte_epargne = CompteEpargne(numero_compte='09876', nom_proprietaire="ABITBOL",
                                                solde=200, pourcentage_interets=constants.POURCENTAGE_INTERETS)

    @patch('poo_comptes.input', return_value=-90)
    def test_retrait_compte_courant_valide(self, input):
        banque(self.mon_compte_courant)
        self.assertEqual(self.mon_compte_courant.afficher_solde(), 10)

    @patch('poo_comptes.input', return_value=-120)
    def test_retrait_compte_courant_avec_agios(self, input):
        banque(self.mon_compte_courant)
        self.assertEqual(self.mon_compte_courant.afficher_solde(), -21.0)

    @patch('poo_comptes.input', return_value=-200)
    def test_retrait_compte_epargne_valide(self, input):
        banque(self.mon_compte_epargne)
        self.assertEqual(self.mon_compte_epargne.afficher_solde(), 0)

    @patch('poo_comptes.input', return_value=100)
    def test_versement_compte_epargne_avec_interets(self, input):
        banque(self.mon_compte_epargne)
        self.assertEqual(self.mon_compte_epargne.afficher_solde(), 315)

    @patch('poo_comptes.input', return_value=100)
    def test_versement_compte_courant(self, input):
        banque(self.mon_compte_courant)
        self.assertEqual(self.mon_compte_courant.afficher_solde(), 200)

    @patch('poo_comptes.input', return_value=-300)
    def test_retrait_compte_courant_invalide(self, input):
        banque(self.mon_compte_courant)
        self.assertEqual(self.mon_compte_courant.afficher_solde(), 100)

    @patch('poo_comptes.input', return_value=-205)
    def test_retrait_compte_epargne_invalide(self, input):
        banque(self.mon_compte_epargne)
        self.assertEqual(self.mon_compte_epargne.afficher_solde(), 200)

    @patch('poo_comptes.input', return_value=100)
    def test_compte_ecrit_fichier_csv(self, input):
        m = unittest.mock.mock_open()
        with patch('poo_comptes.open', m):
            banque(self.mon_compte_courant)
        m.assert_called_once_with(
            f'extraits_comptes/mouvements_compte_{self.mon_compte_courant.numero_compte}.csv', mode='a', newline='')
        handle = m()
        expected = f'{self.mon_compte_courant.numero_compte},{self.mon_compte_courant.nom_proprietaire},{self.mon_compte_courant.solde},{datetime.now().strftime("%d-%m-%Y %H:%M:%S")}\r\n'
        handle.write.assert_called_once_with(expected)
