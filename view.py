import tkinter as tk
from tkinter import messagebox
import constants
import poo_comptes
from model import CompteCourant


class AppliBanque(tk.Tk):
    def __init__(self, compte):
        super().__init__()
        self.compte = compte
        self.title(constants.APP_TITLE)
        self.infos_compte_text = tk.StringVar(value=f"Compte n°{compte.numero_compte} de {compte.nom_proprietaire}")
        self.infos_compte_label = tk.Label(self, textvariable=self.infos_compte_text)
        self.infos_compte_label.grid(column=0, row=0)

        self.compte_label = tk.Label(self, text="Type compte:")
        self.compte_label.grid(column=0, row=1)

        self.type_compte_text = tk.StringVar(value="Courant" if type(compte) == CompteCourant else "Epargne")
        self.type_compte_label = tk.Label(self, textvariable=self.type_compte_text)
        self.type_compte_label.grid(column=1, row=1)

        self.solde_label = tk.Label(self, text="Solde:")
        self.solde_label.grid(column=0, row=2)

        self.solde_text = tk.StringVar(value=compte.solde)
        self.affichage_solde_label = tk.Label(self, textvariable=self.solde_text)
        self.affichage_solde_label.grid(column=1, row=2)

        self.operation_label = tk.Label(self, text="Opération:")
        self.operation_label.grid(column=0, row=3)

        self.choix_operation = tk.StringVar(value=constants.RETRAIT)
        self.radio_retrait = tk.Radiobutton(self, text=constants.RETRAIT, variable=self.choix_operation,
                                            value=constants.RETRAIT)
        self.radio_retrait.grid(column=0, row=4)
        self.radio_versement = tk.Radiobutton(self, text=constants.VERSEMENT, variable=self.choix_operation,
                                              value=constants.VERSEMENT)
        self.radio_versement.grid(column=1, row=4)

        self.montant_label = tk.Label(self, text="Montant:")
        self.montant_label.grid(column=0, row=5)

        self.montant_text = tk.StringVar(value="0")
        self.montant_entry = tk.Entry(self, textvariable=self.montant_text, width=10)
        self.montant_entry.grid(column=1, row=5)

        self.bouton_annuler = tk.Button(self, text="Quitter", command = self.quit)
        self.bouton_annuler.grid(column=0, row=6)

        self.bouton_valider = tk.Button(self, text="Valider", command=self.operation)
        self.bouton_valider.grid(column=1, row=6)

    def operation(self):
        try:
            montant = int(str(self.montant_entry.get()))
            operation_choisie = str(self.choix_operation.get())
            if operation_choisie == constants.RETRAIT:
                montant = -montant
            poo_comptes.operation(compte=self.compte, montant=montant)

            messagebox.showinfo(
                title="Opération: ",
                message=f"Opération choisie: {operation_choisie}\nMontant: {montant}\nSolde: {self.compte.solde} ")
            self.solde_text.set(self.compte.solde)
        except ValueError as e:
            messagebox.showerror(title="Opération: ", message=e)
