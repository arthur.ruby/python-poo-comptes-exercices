"""
constantes pour les taux de banque
"""

POURCENTAGE_INTERETS = 5 / 100
POURCENTAGE_AGIOS = 5 / 100
APP_TITLE = "Banque"
RETRAIT = "Retrait"
VERSEMENT = "Versement"
