"""
Models de l'appli
"""

from abc import ABC, abstractmethod


class Compte(ABC):
    """
    Classe en charge de représenter un compte en banque générique
    """

    def __init__(self, numero_compte, nom_proprietaire, solde=0):
        self.numero_compte = numero_compte
        self.nom_proprietaire = nom_proprietaire
        self.solde = solde

    def retrait(self, somme):
        self.solde += somme

    def versement(self, somme):
        self.solde += somme

    def afficher_solde(self):
        return self.solde


class CompteCourant(Compte):
    """
    classe en charge de représenter un compte courant
    """

    def __init__(self, numero_compte, nom_proprietaire, solde, autorisation_decouvert=0, pourcentage_agios=0):
        super().__init__(numero_compte, nom_proprietaire, solde)
        self.autorisation_decouvert = autorisation_decouvert
        self.pourcentage_agios = pourcentage_agios

    def retrait(self, somme):
        if -self.autorisation_decouvert > self.solde + somme:
            raise ValueError("ERREUR: Vous n'avez pas assez de pognon / de découvert autorisé")
        super().retrait(somme)

    def appliquer_agios(self):
        self.solde += self.solde * self.pourcentage_agios


class CompteEpargne(Compte):
    """
    classe en charge de représenter un compte épargne
    """

    def __init__(self, numero_compte, nom_proprietaire, solde, pourcentage_interets=0):
        super().__init__(numero_compte, nom_proprietaire, solde)
        self.pourcentage_interets = pourcentage_interets

    def retrait(self, somme):
        if self.afficher_solde() + somme < 0:
            raise ValueError("ERREUR: Vous n'avez pas assez de pognon!")
        super().retrait(somme)

    def appliquer_interets(self):
        self.solde += self.solde * self.pourcentage_interets
